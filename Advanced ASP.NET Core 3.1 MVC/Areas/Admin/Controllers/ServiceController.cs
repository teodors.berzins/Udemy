﻿using DataAccess.Data.Repository.IRepository;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Models.ViewModels;
using System;
using System.IO;

namespace Advanced_ASP.NET_Core_3._1_MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ServiceController : Controller
    {
        private readonly IUnitOfWork _unitOFWork;
        private readonly IWebHostEnvironment _hostEnvironment;
        [BindProperty]
        public ServiceVM serviceVM { get; set; }

        public ServiceController(IUnitOfWork unitOFWork, IWebHostEnvironment hostEnvironment)
        {
            _unitOFWork = unitOFWork;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region API Calls

        public IActionResult Upsert(int? id)
        {

            serviceVM = new ServiceVM()
            {
                Service = new Models.Service(),
                CategoryList = _unitOFWork.Category.GetCategoryListForDropdown(),
                FrequencyList = _unitOFWork.Frequency.GetFrequencyListForDropdown()
            };
            if (id is not null)
                serviceVM.Service = _unitOFWork.Service.Get(id.GetValueOrDefault());

            return View(serviceVM);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert()
        {
            if (ModelState.IsValid)
            {
                string webRootPath = _hostEnvironment.WebRootPath;
                var files = HttpContext.Request.Form.Files;
                if (serviceVM.Service.Id == 0)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(webRootPath, @"images\services");
                    var extension = Path.GetExtension(files[0].FileName);
                    using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension), FileMode.Create))
                    {
                        files[0].CopyTo(fileStreams);
                    }
                    serviceVM.Service.ImageUrl = @"\images\services\" + fileName + extension;
                    _unitOFWork.Service.Add(serviceVM.Service);
                }

                else
                {
                    var serviceFromDb = _unitOFWork.Service.Get(serviceVM.Service.Id);
                    if (files.Count > 0)
                    {
                        string fileName = Guid.NewGuid().ToString();
                        var uploads = Path.Combine(webRootPath, @"images\services");
                        var extension_new = Path.GetExtension(files[0].FileName);

                        var imagePath = Path.Combine(webRootPath, serviceFromDb.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(imagePath))
                        {
                            System.IO.File.Delete(imagePath);
                        }

                        using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension_new), FileMode.Create))
                        {
                            files[0].CopyTo(fileStreams);
                        }
                        serviceVM.Service.ImageUrl = @"\images\services\" + fileName + extension_new;

                    }

                    else
                    {
                        serviceVM.Service.ImageUrl = serviceFromDb.ImageUrl;
                    }
                    _unitOFWork.Service.Update(serviceVM.Service);
                }
                _unitOFWork.Save();
                return RedirectToAction(nameof(Index));
            }
            else
                return View(serviceVM);
        }
        public IActionResult GetAll()
        {
            return Json(new { data = _unitOFWork.Service.GetAll(includeProperties: "Category,Frequency") });
        }


        public IActionResult Delete(int id)
        {
            var serviceFromDb = _unitOFWork.Service.Get(id);
            string webRootPath = _hostEnvironment.WebRootPath;

            var imagePath = Path.Combine(webRootPath, serviceFromDb.ImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(imagePath))
            {
                System.IO.File.Delete(imagePath);
            }

            if (serviceFromDb == null)
            {
                return Json(new { success = false, message = "Error while deleting." });
            }
            else
            {
                _unitOFWork.Service.Remove(serviceFromDb);
                _unitOFWork.Save();
                return Json(new { success = true, message = "Deleted successfully." });
            }
        }
        #endregion
    }
}
